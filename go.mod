module gitee.com/gitxuzan/glg

go 1.16

require (
	github.com/goccy/go-json v0.7.1
	github.com/kpango/fastime v1.0.16
	github.com/sirupsen/logrus v1.8.1
	go.uber.org/zap v1.17.0
)
